package com.sda.javagda6.dp.abstractfactory.exercise3.application;

import com.sda.javagda6.dp.abstractfactory.exercise3.Person;
import com.sda.javagda6.dp.abstractfactory.exercise3.application.Application;

import java.time.LocalDateTime;

public class SemesterExtendApplication extends Application {
    private String reason;

    public SemesterExtendApplication(LocalDateTime dateCreation,
                                     String creationLocation,
                                     Person applicant,
                                     String content,
                                     String reason) {
        super(dateCreation, creationLocation, applicant, content);
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
