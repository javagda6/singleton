package com.sda.javagda6.dp.observer.ours;

public class Main {
    public static void main(String[] args) {
        CzatRoom room = new CzatRoom();
        room.connectObserver(new CzatUser("Paweł"));
        room.connectObserver(new CzatUser("Gaweł"));
        room.connectObserver(new CzatUser("Mirek"));
        room.connectObserver(new CzatUser("Irek"));
        room.connectObserver(new CzatUser("Marian"));
        room.connectObserver(new CzatUser("Florian"));

        room.messageArrived("Rudy sie zeni");
    }
}
