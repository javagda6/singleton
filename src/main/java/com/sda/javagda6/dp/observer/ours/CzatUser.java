package com.sda.javagda6.dp.observer.ours;

public class CzatUser implements IObserver {

    private String name;

    public CzatUser(String name) {
        this.name = name;
    }

    @Override
    public void notifyObserver(String aboutWhat) {
        System.out.println("Zostaje powiadomiony " + name + " wiadomosc: " + aboutWhat);

    }
}
