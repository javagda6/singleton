package com.sda.javagda6.dp.singleton.ex2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Game g = new Game();
        SettingsReader reader = new SettingsReader();
        reader.readSettingsFromFile();

        Scanner scanner = new Scanner(System.in);
        while (!g.hasEnded()){
            g.nextRound();
            int userResult = scanner.nextInt();
            if(g.validate(userResult)){
                System.out.println("OK!");
            }else{
                System.out.println("NOT OK!");
            }
        }
    }
}
