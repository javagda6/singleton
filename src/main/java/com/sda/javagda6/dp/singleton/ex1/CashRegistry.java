package com.sda.javagda6.dp.singleton.ex1;

public class CashRegistry {
    public Ticket generateTicket() {
        return new Ticket(TicketGenerator.instance.getCounter(), "Cash Ticket");
    }
}
