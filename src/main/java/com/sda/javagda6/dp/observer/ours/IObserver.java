package com.sda.javagda6.dp.observer.ours;

/**
 * Ten interfejs implementuja wszystkie klasy ktore maja byc powiadamiane
 * po zmianie/o dotarciu wiadomosci.
 */
public interface IObserver {
    void notifyObserver(String aboutWhat);
}
