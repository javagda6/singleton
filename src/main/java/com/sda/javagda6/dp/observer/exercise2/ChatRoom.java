package com.sda.javagda6.dp.observer.exercise2;

import java.util.*;

public class ChatRoom extends Observable {
    private int userCounter = 0;
    private Map<Integer, ChatUser> userMap = new HashMap<>();
    private String roomName;
    private List<String> adminNicks = new ArrayList<>(Arrays.asList("admin"));

    public ChatRoom(String roomName) {
        this.roomName = roomName;
    }

    public void addUser(String nick) {
        ChatUser newUser = new ChatUser(userCounter++, nick);

        for (ChatUser user : userMap.values()) {
            if (user.getNick().equals(newUser.getNick())) {
                System.out.println("User with that nick already exists");
                return;
            }
        }

        if (adminNicks.contains(newUser.getNick())) {
            // ustawiamy mu admina
            newUser.setAdmin(true);
        }

        userMap.put(newUser.getId(), newUser);
        addObserver(newUser);
    }

    // poklikash
    public void sendMessage(int senderId, String message) {
        setChanged();
        notifyObservers(new Message(senderId, message));
    }
}
