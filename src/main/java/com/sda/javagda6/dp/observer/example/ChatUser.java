package com.sda.javagda6.dp.observer.example;

import java.util.Observable;
import java.util.Observer;

public class ChatUser implements Observer {
    private String name;

    public ChatUser(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable observable, Object o) {

        String strValueOf = String.valueOf(o); // 1
//        String strValueOf = o.toString(); // 2

        if (o instanceof String) {
            String casted = (String) o;
            System.out.println(name + " powiadomiony o " + casted);
        } else if (o instanceof Meeting) {
            Meeting meeting = (Meeting) o;
            System.out.println(name + " powiadomiony o meetingu: " + meeting.getWtf());
        } else {
            System.out.println("Niepoprawna wiadomosc.");
        }
    }
}
