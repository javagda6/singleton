package com.sda.javagda6.dp.builder;

public class Main {
    public static void main(String[] args) {
        Hero.Builder builder = new Hero.Builder();

        builder.setName("Marian").setHealth(100).setMana(1000);
//        builder.setHealth(100);
//        builder.setMana(1000);
        //...
        Hero hero = builder.createHero();

        builder.setFathersName("Jurek").setStealth(2000).setHealth(6000);
        Hero hero2 = builder.createHero();

        System.out.println(hero);
        System.out.println(hero2);
    }
}
