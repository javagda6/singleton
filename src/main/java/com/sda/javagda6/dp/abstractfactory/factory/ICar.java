package com.sda.javagda6.dp.abstractfactory.factory;

public interface ICar {

    int getSpeed();

    String getModel();

    String getManufacturer();

    int getHorsepower();
}
