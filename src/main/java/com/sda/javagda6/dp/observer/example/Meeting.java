package com.sda.javagda6.dp.observer.example;

public class Meeting {
    private String wtf;

    public Meeting(String wtf) {
        this.wtf = wtf;
    }

    public String getWtf() {
        return wtf;
    }

    public void setWtf(String wtf) {
        this.wtf = wtf;
    }
}
