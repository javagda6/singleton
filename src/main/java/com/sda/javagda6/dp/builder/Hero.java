package com.sda.javagda6.dp.builder;

public class Hero {
    private int health, mana, stealth, stamina;
    private String name, surname, mothersName, fathersName;

    public Hero(int health, int mana, int stealth, int stamina, String name, String surname, String mothersName, String fathersName) {
        this.health = health;
        this.mana = mana;
        this.stealth = stealth;
        this.stamina = stamina;
        this.name = name;
        this.surname = surname;
        this.mothersName = mothersName;
        this.fathersName = fathersName;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "health=" + health +
                ", mana=" + mana +
                ", stealth=" + stealth +
                ", stamina=" + stamina +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", mothersName='" + mothersName + '\'' +
                ", fathersName='" + fathersName + '\'' +
                '}';
    }

    public int getHealth() {
        return health;
    }

    public int getMana() {
        return mana;
    }

    public int getStealth() {
        return stealth;
    }

    public int getStamina() {
        return stamina;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMothersName() {
        return mothersName;
    }

    public String getFathersName() {
        return fathersName;
    }

    public static class Builder{

        private int health=100;
        private int mana;
        private int stealth;
        private int stamina;
        private String name;
        private String surname;
        private String mothersName;
        private String fathersName;

        public Builder setHealth(int health) {
            this.health = health;
            return this;
        }

        public Builder setMana(int mana) {
            this.mana = mana;
            return this;
        }

        public Builder setStealth(int stealth) {
            this.stealth = stealth;
            return this;
        }

        public Builder setStamina(int stamina) {
            this.stamina = stamina;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder setMothersName(String mothersName) {
            this.mothersName = mothersName;
            return this;
        }

        public Builder setFathersName(String fathersName) {
            this.fathersName = fathersName;
            return this;
        }

        public Hero createHero() {
            return new Hero(health, mana, stealth, stamina, name, surname, mothersName, fathersName);
        }
    }
}
