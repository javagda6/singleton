package com.sda.javagda6.dp.abstractfactory.exercise3.application;

import com.sda.javagda6.dp.abstractfactory.exercise3.Person;
import com.sda.javagda6.dp.abstractfactory.exercise3.application.Application;

import java.time.LocalDateTime;
import java.util.List;

public class ConditionalStayApplication extends Application {
    private List<Double> grades;
    private String reason;

    public ConditionalStayApplication(LocalDateTime dateCreation,
                                      String creationLocation,
                                      Person applicant,
                                      String content,
                                      List<Double> grades,
                                      String reason) {
        super(dateCreation, creationLocation, applicant, content);
        this.grades = grades;
        this.reason = reason;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
