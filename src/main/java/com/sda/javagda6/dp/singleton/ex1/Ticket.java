package com.sda.javagda6.dp.singleton.ex1;

public class Ticket {
    private int id;
    private String ticketCase;

    public Ticket(int id, String ticketCase) {
        this.id = id;
        this.ticketCase = ticketCase;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTicketCase() {
        return ticketCase;
    }

    public void setTicketCase(String ticketCase) {
        this.ticketCase = ticketCase;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", ticketCase='" + ticketCase + '\'' +
                '}';
    }
}
