package com.sda.javagda6.dp.singleton.ex1;

public class WaitingRoom {
    public Ticket generate(){
        return new Ticket(TicketGenerator.instance.getCounter(), "WaitingRoom Ticket");
    }
}
