package com.sda.javagda6.dp.observer.example;

import java.util.Observable;

public class ChatRoom extends Observable {


    // nie piszemy żadnej kolekcji
    // posługujemy sie metodą (z klasy Observable)
    // addObserver
    // removeObserver

    public void addUser(ChatUser user) {
        // if(map.containsKey(user.getName()){
        // sout(Error)
        // }
        addObserver(user);
        // map.put(user.getName(), user);
    }

    public void notifyAboutMessage(String myMessage){
        setChanged();
        notifyObservers(myMessage);
    }

    public void notifyAboutMeeting(Meeting m){
        setChanged();
        notifyObservers(m);
    }
}
