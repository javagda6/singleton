package com.sda.javagda6.dp.observer.ours;

import java.util.HashSet;
import java.util.Set;

public class OurObservable {

    private Set<IObserver> observers;

    public OurObservable() {
        this.observers = new HashSet<>();
    }

    public void connectObserver(IObserver newObserver) {
        this.observers.add(newObserver);
    }

    public void removeObserver(IObserver toRemove) {
        this.observers.remove(toRemove);
    }

    public void notifyAllObservers(String aboutWhat) {
        for (IObserver singleObserver : observers) {
            singleObserver.notifyObserver(aboutWhat);
        }
    }
}
