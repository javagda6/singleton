package com.sda.javagda6.dp.observer.exercise2;

public class Main {
    public static void main(String[] args) {
        ChatRoom room = new ChatRoom("rum");
        room.addUser("admin");
        room.addUser("bartek");
        room.addUser("michau");

        room.sendMessage(1, "wiad");
    }
}
