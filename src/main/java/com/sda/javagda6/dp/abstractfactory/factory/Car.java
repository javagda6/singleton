package com.sda.javagda6.dp.abstractfactory.factory;

class Car implements ICar {
    private String model;
    private int speed;
    private int horsepower;
    private String manufacturer;

    private boolean hasLPG;


    protected Car(String model, int speed, int horsepower, String manufacturer, boolean hasLPG) {
        this.model = model;
        // jeśli speed > 250
        // to zwroc 250
        // w przeciwnym razie zwroc speed
        // alt:
        // w przeciwnym razie jesli speed < 70
        // to zwroc 70
        // w przeciwnym razie zwroc speed
        this.speed = ((speed > 250) ? 250 : ((speed < 70) ? 70 : speed));
        this.horsepower = horsepower;
        this.manufacturer = manufacturer;
        this.hasLPG = hasLPG;
    }

    @Override
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    @Override
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public boolean isHasLPG() {
        return hasLPG;
    }

    public void setHasLPG(boolean hasLPG) {
        this.hasLPG = hasLPG;
    }
}
