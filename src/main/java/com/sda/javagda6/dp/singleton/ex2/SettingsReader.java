package com.sda.javagda6.dp.singleton.ex2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOError;
import java.io.IOException;

public class SettingsReader {
    public void readSettingsFromFile() {
        try (BufferedReader reader = new BufferedReader(new FileReader("input.txt"))) {

            String line1 = reader.readLine();
            String line2 = reader.readLine();
            String line3 = reader.readLine();
            String line4 = reader.readLine();

            String value1 = line1.split("=")[1];
            String value2 = line2.split("=")[1];
            String value3 = line3.split("=")[1];
            String value4 = line4.split("=")[1];

            int range1= Integer.parseInt(value1);
            int range2= Integer.parseInt(value2);
            int rounds = Integer.parseInt(value4);

            MySettings.INSTANCE.setRange1(range1);
            MySettings.INSTANCE.setRange2(range2);
            MySettings.INSTANCE.setNumberOfRounds(rounds);
            MySettings.INSTANCE.setSigns(value3);

        } catch (IOException ioe) {
            System.err.println("brak pliku");
        }
    }
}
