package com.sda.javagda6.dp.observer.exercise2;

public class Message {
    private int senderId;
    private String message;

    public Message(int id, String message) {
        this.senderId = id;
        this.message = message;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "senderId=" + senderId +
                ", message='" + message + '\'' +
                '}';
    }
}
