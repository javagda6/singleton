package com.sda.javagda6.dp.abstractfactory.factory;

public abstract class AbstractCarFactory {

    public static Car createBMWM3() {
        return new Car("M3", 230, 100, "BMW", true);
    }

    public static Car createBMW3() {
        return new Car("3", 150, 213, "BMW", true);
    }

    public static Car createAudiA4() {
        return new Car("A4", 200, 120, "Audi", false);
    }

    public static Car createPolonez(int speed) {
        return new Car("Polonez", speed, 60, "FSO", true);
    }
}
