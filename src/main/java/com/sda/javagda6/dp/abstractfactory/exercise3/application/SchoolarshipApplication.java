package com.sda.javagda6.dp.abstractfactory.exercise3.application;

import com.sda.javagda6.dp.abstractfactory.exercise3.Person;
import com.sda.javagda6.dp.abstractfactory.exercise3.application.Application;

import java.time.LocalDateTime;
import java.util.List;

public class SchoolarshipApplication extends Application {

    private List<String> extracurricularActivities;
    private List<Double> grades;

    public SchoolarshipApplication(LocalDateTime dateCreation,
                                   String creationLocation,
                                   Person applicant,
                                   String content,
                                   List<String> extracurricularActivities,
                                   List<Double> grades) {
        super(dateCreation, creationLocation, applicant, content);
        this.extracurricularActivities = extracurricularActivities;
        this.grades = grades;
    }

    public List<String> getExtracurricularActivities() {
        return extracurricularActivities;
    }

    public void setExtracurricularActivities(List<String> extracurricularActivities) {
        this.extracurricularActivities = extracurricularActivities;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }
}
