package com.sda.javagda6.dp.singleton.ex1;

public class TicketGenerator {
    // sposób 0 - enum
//    INSTANCE;

    // sposób 1 - eager
    public static final TicketGenerator instance = new TicketGenerator();

    // sposób 2 - eager
//    private static final TicketGenerator instance = new TicketGenerator();
//    public static TicketGenerator getInstance() {
//        return instance;
//    }

    // sposób 3 - lazy (single thread)
//    private static TicketGenerator instance;
//    public static TicketGenerator getInstance() {
//        if (instance == null) {
//            instance = new TicketGenerator();
//        }
//        return instance;
//    }

    // sposób 4 - lazy (multi fred)
//    private static TicketGenerator instance;
//    public static synchronized TicketGenerator getInstance() {
//        if (instance == null) {
//            synchronized (TicketGenerator.class) {
//                if (instance == null) {
//                    instance = new TicketGenerator();
//                }
//            }
//        }
//        return instance;
//    }

    // koniecznie uprywatnić konstruktor
    private TicketGenerator() {
    }
    private int counter = 0;

    public int getCounter() {
        return ++counter;
    }

}
