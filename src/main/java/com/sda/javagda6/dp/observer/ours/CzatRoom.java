package com.sda.javagda6.dp.observer.ours;

public class CzatRoom extends OurObservable {

    public void messageArrived(String aboutWhat){
        this.notifyAllObservers(aboutWhat);
    }
}
