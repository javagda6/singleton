package com.sda.javagda6.dp.observer.example;

import com.sda.javagda6.dp.observer.ours.CzatRoom;
import com.sda.javagda6.dp.observer.ours.CzatUser;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<ChatUser> userList = new LinkedList<>(Arrays.asList(new ChatUser("admin"), null));

        ChatRoom room = new ChatRoom();

        room.addObserver(new ChatUser("Paweł"));
        room.addObserver(new ChatUser("Gaweł"));
        room.addObserver(new ChatUser("Mirek"));
        room.addObserver(new ChatUser("Irek"));
        room.addObserver(new ChatUser("Marian"));
        room.addObserver(new ChatUser("Florian"));

        room.notifyAboutMessage("Rudy sie zeni");

        room.notifyAboutMeeting(new Meeting("Meeting at pawels"));
    }
}
